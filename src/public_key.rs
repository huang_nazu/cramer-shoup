use digest::Digest;
use num_bigint::{BigUint, RandBigInt};
use rand_core::CryptoRngCore;

use crate::{
    ciphertext::Ciphertext, error::Error, params::CramerShoupParams, utils::compute_alpha,
    PrivateKey,
};

/// Represents the public part of an Cramer-Shoup key.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct PublicKey {
    c: BigUint,
    d: BigUint,
    h: BigUint,
}

impl PublicKey {
    /// Constructs a Cramer-Shoup public key from the individual components.
    pub fn from_components(c: BigUint, d: BigUint, h: BigUint) -> Self {
        Self { c, d, h }
    }

    /// Get the public key from the private key according to the given parameters.
    pub fn from_private_key<H: Digest + Clone>(
        sk: PrivateKey,
        params: &CramerShoupParams<H>,
    ) -> Self {
        sk.to_public_key(params)
    }

    /// Encrypt the given plaintext bytes according to the given parameters.
    pub fn encrypt<R: CryptoRngCore, H: Digest + Clone>(
        &self,
        rng: &mut R,
        msg: &[u8],
        params: &CramerShoupParams<H>,
    ) -> Result<Vec<u8>, Error> {
        let msg = BigUint::from_bytes_le(msg);
        if msg.bits() as usize <= params.bits {
            self.encrypt_core(rng, msg, params)
                .map(|c| c.to_bytes(params.bits))
        } else {
            Err(Error::MessageTooLong)
        }
    }

    /// Encrypt the given plaintext integer according to the given parameters.
    pub fn encrypt_core<R: CryptoRngCore, H: Digest + Clone>(
        &self,
        rng: &mut R,
        msg: BigUint,
        params: &CramerShoupParams<H>,
    ) -> Result<Ciphertext, Error> {
        let (c, d, h) = (&self.c, &self.d, &self.h);
        let (p, g1, g2) = (&params.p, &params.g1, &params.g2);

        if msg.cmp(p).is_lt() {
            let k = rng.gen_biguint_below(p);

            let u1 = g1.modpow(&k, p);
            let u2 = g2.modpow(&k, p);
            let e = (h.modpow(&k, p) * msg) % p;
            let alpha = compute_alpha::<H>(&u1, &u2, &e, params.bits);
            let v = (c.modpow(&k, p) * d.modpow(&(k * &alpha), p)) % p;

            Ok(Ciphertext::from_components(u1, u2, e, v))
        } else {
            Err(Error::MessageTooLong)
        }
    }
}
