use num_bigint::BigUint;

use crate::error::Error;

/// Represents the ciphertext of Cramer-Shoup cryptosystem
#[derive(Debug,Clone, PartialEq, Eq)]
pub struct Ciphertext {
    u1: BigUint,
    u2: BigUint,
    e: BigUint,
    v: BigUint,
}

impl Ciphertext {
    /// Constructs a Cramer-Shoup ciphertext from the individual components.
    pub fn from_components(u1: BigUint, u2: BigUint, e: BigUint, v: BigUint) -> Self {
        Self { u1, u2, e, v }
    }

    /// Deconstructs a Cramer-Shoup ciphertext into the individual components.
    pub fn into_components(self) -> (BigUint, BigUint, BigUint, BigUint) {
        (self.u1, self.u2, self.e, self.v)
    }

    /// Convert the ciphertext with each elements being the given `bits` into byte strings
    pub fn to_bytes(&self, bits: usize) -> Vec<u8> {
        let mut u1 = self.u1.to_bytes_le();
        let mut u2 = self.u2.to_bytes_le();
        let mut e = self.e.to_bytes_le();
        let mut v = self.v.to_bytes_le();

        u1.resize(bits / 8, 0);
        u2.resize(bits / 8, 0);
        e.resize(bits / 8, 0);
        v.resize(bits / 8, 0);
        [u1, u2, e, v].concat()
    }

    /// Convert the byte strings into the ciphertext with each elements being the given `bits`.
    pub fn try_from_bytes(bytes: &[u8], bits: usize) -> Result<Self, Error> {
        if bytes.len() == bits / 8 * 4 {
            let u1 = BigUint::from_bytes_le(&bytes[..bits / 8]);
            let u2 = BigUint::from_bytes_le(&bytes[bits / 8..bits / 8 * 2]);
            let e = BigUint::from_bytes_le(&bytes[bits / 8 * 2..bits / 8 * 3]);
            let v = BigUint::from_bytes_le(&bytes[bits / 8 * 3..bits / 8 * 4]);

            Ok(Self { u1, u2, e, v })
        } else {
            Err(Error::InvalidCiphertextLength)
        }
    }
}

#[cfg(test)]
mod tests{
    use num_bigint::BigUint;
    use num_traits::Zero;

    use crate::ciphertext::Ciphertext;

    #[test]
    fn components() {
        let c = Ciphertext {
            u1: BigUint::zero(),
            u2: BigUint::zero(),
            e: BigUint::zero(),
            v: BigUint::zero(),
        };

        let (u1,u2,e,v) = c.clone().into_components();
        assert_eq!(c, Ciphertext::from_components(u1,u2,e,v));
    }

    #[test]
    fn bytes() {
        let c = Ciphertext {
            u1: BigUint::zero(),
            u2: BigUint::zero(),
            e: BigUint::zero(),
            v: BigUint::zero(),
        };

        let bits = 1024;
        let b = c.to_bytes(bits);
        assert_eq!(c, Ciphertext::try_from_bytes(&b, bits).unwrap());
    }
}