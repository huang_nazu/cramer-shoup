use digest::Digest;
use num_bigint::{BigUint, RandBigInt};
use rand_core::CryptoRngCore;

use crate::{
    ciphertext::Ciphertext,
    error::Error,
    params::CramerShoupParams,
    public_key::PublicKey,
    utils::{compute_alpha, inv},
};

/// Represents the private part of an Cramer-Shoup key.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct PrivateKey {
    x1: BigUint,
    x2: BigUint,
    y1: BigUint,
    y2: BigUint,
    z: BigUint,
}

impl PrivateKey {
    /// Generate a new Cramer-Shoup private key according to the given parameters.
    pub fn new<R: CryptoRngCore + ?Sized, H: Digest + Clone>(
        rng: &mut R,
        params: &CramerShoupParams<H>,
    ) -> Self {
        let p = &params.p;
        let x1 = rng.gen_biguint_below(p);
        let x2 = rng.gen_biguint_below(p);
        let y1 = rng.gen_biguint_below(p);
        let y2 = rng.gen_biguint_below(p);
        let z = rng.gen_biguint_below(p);

        Self { x1, x2, y1, y2, z }
    }

    /// Constructs a Cramer-Shoup private key from the individual components.
    pub fn from_components(x1: BigUint, x2: BigUint, y1: BigUint, y2: BigUint, z: BigUint) -> Self {
        Self { x1, x2, y1, y2, z }
    }

    /// Get the public key from the private key according to the given parameters.
    pub fn to_public_key<H: Digest + Clone>(&self, params: &CramerShoupParams<H>) -> PublicKey {
        let (x1, x2, y1, y2, z) = (&self.x1, &self.x2, &self.y1, &self.y2, &self.z);
        let (p, g1, g2) = (&params.p, &params.g1, &params.g2);

        let c = (g1.modpow(x1, p) * g2.modpow(x2, p)) % p;
        let d = (g1.modpow(y1, p) * g2.modpow(y2, p)) % p;
        let h = g1.modpow(z, p);

        PublicKey::from_components(c, d, h)
    }

    /// Decrypt the given ciphertext bytes according to the given parameters.
    pub fn decrypt<H: Digest + Clone>(
        &self,
        ciphertext: &[u8],
        params: &CramerShoupParams<H>,
    ) -> Result<Vec<u8>, Error> {
        let ciphertext = Ciphertext::try_from_bytes(ciphertext, params.bits)?;
        self.decrypt_core(ciphertext, params).map(|m| {
            let mut m = m.to_bytes_le();
            m.resize(params.bits / 8, 0);
            m
        })
    }

    /// Decrypt the given ciphertext struct according to the given parameters.
    pub fn decrypt_core<H: Digest + Clone>(
        &self,
        ciphertext: Ciphertext,
        params: &CramerShoupParams<H>,
    ) -> Result<BigUint, Error> {
        let (x1, x2, y1, y2, z) = (&self.x1, &self.x2, &self.y1, &self.y2, &self.z);
        let p = &params.p;
        let (u1, u2, e, v) = ciphertext.into_components();

        let alpha = compute_alpha::<H>(&u1, &u2, &e, params.bits);

        // u1 ** (x1 + y1 * alpha) * u2 ** (x2 + y2 * alpha)
        let validation =
            (u1.modpow(&(x1 + y1 * &alpha), p) * u2.modpow(&(x2 + y2 * &alpha), p)) % p;
        if validation != v {
            Err(Error::Decryption)
        } else {
            let u1z = u1.modpow(z, p);
            let u1z_inv = inv(u1z, p.clone());
            let m = (e * u1z_inv) % p;

            Ok(m)
        }
    }
}

impl From<&PrivateKey> for PrivateKey {
    fn from(value: &PrivateKey) -> Self {
        value.clone()
    }
}
