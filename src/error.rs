use thiserror::Error;

#[derive(Debug,Error)]
pub enum Error {
    #[error("Message too long")]
    MessageTooLong,
    #[error("Decryption error")]
    Decryption,
    #[error("Invalid ciphertext length")]
    InvalidCiphertextLength,
}