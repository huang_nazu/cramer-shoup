use digest::Digest;
use num_bigint::{BigInt, BigUint};
use num_integer::Integer;

pub fn compute_alpha<H: Digest>(
    u1: &BigUint,
    u2: &BigUint,
    e: &BigUint,
    bits: usize,
) -> BigUint {
    let mut u1 = u1.to_bytes_le();
    let mut u2 = u2.to_bytes_le();
    let mut e = e.to_bytes_le();
    u1.resize(bits/8, 0);
    u2.resize(bits/8, 0);
    e.resize(bits/8, 0);

    let hash = H::digest(&[u1, u2, e].concat());
    BigUint::from_bytes_le(&hash)
}

pub fn inv(a: BigUint, p: BigUint) -> BigUint {
    let a = BigInt::from(a);
    let p = BigInt::from(p);

    ((a.extended_gcd(&p).x + &p) % p).to_biguint().unwrap()
}
