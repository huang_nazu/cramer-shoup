use std::marker::PhantomData;

use digest::Digest;
use num_bigint::BigUint;
use once_cell::sync::Lazy;
use sha3::Sha3_256;

/// Represents the public parameters of Cramer-Shoup cryptosystem
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CramerShoupParams<H: Digest + Clone> {
    pub p: BigUint,
    pub g1: BigUint,
    pub g2: BigUint,
    pub bits: usize,
    _marker: PhantomData<H>,
}

impl<H: Digest + Clone> CramerShoupParams<H> {
    /// Constructs a Cramer-Shoup public parameters from the individual components.
    pub fn new(p: BigUint, g1: BigUint, g2: BigUint, bits: usize) -> Self {
        Self {
            p,
            g1,
            g2,
            bits,
            _marker: PhantomData,
        }
    }
}

/// FFDHE2048 group parameters with additional generator `8` and `SHA3-256`.
/// This is the testing parameter which is made because there is no standard parameter for Cramer-Shoup cryptosystem.
pub static FFDHE2048_8_SHA3_256: Lazy<CramerShoupParams<Sha3_256>> = Lazy::new(|| {
    // p = 2^2048 - 2^1984 + {[2^1918 * e] + 560316 } * 2^64 - 1
    let p = BigUint::parse_bytes(b"FFFFFFFFFFFFFFFFADF85458A2BB4A9AAFDC5620273D3CF1D8B9C583CE2D3695A9E13641146433FBCC939DCE249B3EF97D2FE363630C75D8F681B202AEC4617AD3DF1ED5D5FD65612433F51F5F066ED0856365553DED1AF3B557135E7F57C935984F0C70E0E68B77E2A689DAF3EFE8721DF158A136ADE73530ACCA4F483A797ABC0AB182B324FB61D108A94BB2C8E3FBB96ADAB760D7F4681D4F42A3DE394DF4AE56EDE76372BB190B07A7C8EE0A6D709E02FCE1CDF7E2ECC03404CD28342F619172FE9CE98583FF8E4F1232EEF28183C3FE3B1B4C6FAD733BB5FCBC2EC22005C58EF1837D1683B2C6F34A26C1B2EFFA886B423861285C97FFFFFFFFFFFFFFFF", 16).unwrap();
    // q = (p-1)/2
    let _q = BigUint::parse_bytes(b"7FFFFFFFFFFFFFFFD6FC2A2C515DA54D57EE2B10139E9E78EC5CE2C1E7169B4AD4F09B208A3219FDE649CEE7124D9F7CBE97F1B1B1863AEC7B40D901576230BD69EF8F6AEAFEB2B09219FA8FAF83376842B1B2AA9EF68D79DAAB89AF3FABE49ACC278638707345BBF15344ED79F7F4390EF8AC509B56F39A98566527A41D3CBD5E0558C159927DB0E88454A5D96471FDDCB56D5BB06BFA340EA7A151EF1CA6FA572B76F3B1B95D8C8583D3E4770536B84F017E70E6FBF176601A0266941A17B0C8B97F4E74C2C1FFC7278919777940C1E1FF1D8DA637D6B99DDAFE5E17611002E2C778C1BE8B41D96379A51360D977FD4435A11C30942E4BFFFFFFFFFFFFFFFF", 16).unwrap();
    // g_1 = 2
    let g1 = 2u8.into();
    // g_2 = 8
    let g2 = 8u8.into();

    CramerShoupParams::new(p, g1, g2, 2048)
});

#[cfg(test)]
mod tests {
    use crate::params::FFDHE2048_8_SHA3_256;

    #[test]
    fn preview_size() {
        let p = FFDHE2048_8_SHA3_256.p.bits();
        println!("p: {0} bits", p);
    }
}
