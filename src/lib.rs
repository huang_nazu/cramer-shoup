//! # Cramer-Shoup cryptosystem
//! A Cramer-Shoup cryptosystem implementation in pure Rust.
//! ## Example
//! ```
//! use cramer_shoup::{PublicKey};
//! 
//! ```

mod params;
mod private_key;
mod public_key;
mod ciphertext;
mod error;
mod utils;
mod extended_public_key;
mod extended_private_key;

pub use params::{CramerShoupParams,FFDHE2048_8_SHA3_256};
pub use private_key::PrivateKey;
pub use public_key::PublicKey;
pub use extended_private_key::ExtendedPrivateKey;
pub use extended_public_key::ExtendedPublicKey;
pub use error::Error;

pub use num_bigint::BigUint;

#[cfg(test)]
mod tests {
    use rand::Rng;

    use crate::{private_key::PrivateKey, params};

    #[test]
    fn encdec() {
        let mut rng = rand::thread_rng();
        let params = &params::FFDHE2048_8_SHA3_256;
        let sk = PrivateKey::new(&mut rng,params);
        let pk = sk.to_public_key(params);
        
        let m:Vec<u8> = (0..params.bits/8).map(|_| rng.gen()).collect();
        let c = pk.encrypt(&mut rng, &m, params).unwrap();
        assert_eq!(m,sk.decrypt(&c, params).unwrap());
    }
}