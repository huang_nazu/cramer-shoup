use crate::{CramerShoupParams, Error, PrivateKey, ciphertext::Ciphertext};
use digest::Digest;
use num_bigint::BigUint;
use rand_core::CryptoRngCore;

/// Represents the private part of an Cramer-Shoup key with parameters.
pub struct ExtendedPrivateKey<H: Digest + Clone> {
    pub params: CramerShoupParams<H>,
    pub private_key: PrivateKey,
}

impl<H: Digest + Clone> ExtendedPrivateKey<H> {
    /// Generate a new Cramer-Shoup private key according to the given parameters.
    pub fn new<R: CryptoRngCore + ?Sized>(rng: &mut R, params: &CramerShoupParams<H>) -> Self {
        let private_key = PrivateKey::new(rng, &params);

        Self {
            params: params.clone(),
            private_key,
        }
    }

    /// Constructs a Cramer-Shoup private key with parameters from the individual components.
    pub fn from_components(private_key: PrivateKey, params: &CramerShoupParams<H>) -> Self {
        Self {
            params: params.clone(),
            private_key,
        }
    }

    /// Decrypt the given ciphertext bytes.
    pub fn decrypt(&self, ciphertext: &[u8]) -> Result<Vec<u8>, Error> {
        self.private_key.decrypt(ciphertext, &self.params)
    }

    /// Decrypt the given ciphertext struct.
    pub fn decrypt_core(&self, ciphertext: Ciphertext) -> Result<BigUint, Error> {
        self.private_key.decrypt_core(ciphertext, &self.params)
    }
}
