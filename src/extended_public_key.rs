use crate::{extended_private_key::ExtendedPrivateKey, CramerShoupParams, Error, PublicKey, ciphertext::Ciphertext};
use digest::Digest;
use num_bigint::BigUint;
use rand_core::CryptoRngCore;

/// Represents the public part of an Cramer-Shoup key with parameters.
pub struct ExtendedPublicKey<H: Digest + Clone> {
    pub params: CramerShoupParams<H>,
    pub public_key: PublicKey,
}

impl<H: Digest + Clone> ExtendedPublicKey<H> {
    /// Constructs a Cramer-Shoup public key with parameters from the individual components.
    pub fn from_components(public_key: PublicKey, params: &CramerShoupParams<H>) -> Self {
        Self {
            params: params.clone(),
            public_key,
        }
    }

    /// Encrypt the given plaintext bytes.
    pub fn encrypt<R: CryptoRngCore>(&self, rng: &mut R, msg: &[u8]) -> Result<Vec<u8>, Error> {
        self.public_key.encrypt(rng, msg, &self.params)
    }

    /// Encrypt the given plaintext integer.
    pub fn encrypt_core<R: CryptoRngCore>(&self, rng: &mut R, msg: BigUint) -> Result<Ciphertext, Error> {
        self.public_key.encrypt_core(rng, msg, &self.params)
    }
}

impl<H: Digest + Clone> From<ExtendedPrivateKey<H>> for ExtendedPublicKey<H> {
    fn from(value: ExtendedPrivateKey<H>) -> Self {
        Self::from(&value)
    }
}

impl<H: Digest + Clone> From<&ExtendedPrivateKey<H>> for ExtendedPublicKey<H> {
    fn from(value: &ExtendedPrivateKey<H>) -> Self {
        let public_key = value.private_key.to_public_key(&value.params);
        
        Self {
            params: value.params.clone(),
            public_key,
        }
    }
}
